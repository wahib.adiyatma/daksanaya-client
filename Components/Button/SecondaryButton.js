import { Button } from '@chakra-ui/button';

const SecondaryButton = ({ children, ...props }) => (
    <Button
        bg="linear-gradient(180deg, rgba(80, 139, 255, 0.76) 13.02%, #0B62FF 68.23%)"
        borderRadius="10px"
        color="white"
        fontSize={{ lg: '1.25rem', base: '1rem' }}
        fontWeight={700}
        _hover={{
            transform: "scale(1.07)",
        }}
        _focus={{ outline: 'none' }}
        _active={{ bg: 'linear-gradient(180deg, rgba(80, 139, 255, 0.76) 13.02%, #0B62FF 68.23%)' }}
        {...props}
    >
        {children}
    </Button>
);
export default SecondaryButton;