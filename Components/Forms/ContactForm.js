import { Grid } from "@chakra-ui/layout";
import { ErrorToast, SuccessToast } from '../Toast';
import {
    FormControl,
    FormLabel,
} from "@chakra-ui/react"
import { Input } from "@chakra-ui/input";
import { contactApi } from "../../_api/service";
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { Flex } from '@chakra-ui/layout';
import PrimaryButton from '../Button/PrimaryButton'
import { toastErrorMessage } from '../../utils/functions';
import { REGEX_EMAIL } from '../../constants/regex';
import { Textarea } from "@chakra-ui/textarea";

const ContactForm = () => {
    const {
        register,
        getValues,
        trigger,
        formState: { errors },
    } = useForm();
    const validate = {
        required: { value: true, message: 'Silakan isi seluruh field' },
    };

    const [isLoading, setIsLoading] = useState(false);

    const onSubmit = async (e) => {
        e?.preventDefault();

        if (!(await trigger())) {
            toastErrorMessage({ errors, title: 'Gagal mengirim pesan' });
            return;
        }

        setIsLoading(true);
        contactApi
            .addContact({ ...getValues() })
            .then(() => SuccessToast('Pesan berhasil dikirim'))
            .catch((err) => ErrorToast('Gagal mengirim pesan'))
            .finally(() => setIsLoading(false));
    };

    return (
        <>
            <Flex
                flexDir="column"
                justifyContent="center"
                alignItems="left"
                mx={{ lg: "40px" }}
            >
                <Grid
                    templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(2, 1fr)' }}
                    columnGap="14px"
                    rowGap={{ base: '.75rem', md: '0' }}
                    mb="0.75rem"
                    w={{ lg: "500px", base: '250px' }}
                >
                    <FormControl>
                        <FormLabel >Nama :</FormLabel>
                        <Input
                            text="Nama"
                            {...register('name', { ...validate })}
                        />

                    </FormControl>
                    <FormControl>
                        <FormLabel>Alamat Email :</FormLabel>
                        <Input
                            text="Email"
                            {...register('email', {
                                ...validate,
                                pattern: {
                                    value: REGEX_EMAIL,
                                    message: 'Email tidak valid',
                                },
                            })}
                        />

                    </FormControl>
                </Grid>
                <FormControl>
                    <FormLabel>Isi Pesan :</FormLabel>
                    <Textarea
                        text="Pesan"
                        h="10rem"
                        {...register('message', { ...validate })}

                    />
                </FormControl>
                <Flex
                    align="right"
                >
                    <PrimaryButton
                        w="6rem"
                        h="2.25rem"
                        mt="1.25rem"
                        fontSize="1rem"
                        onClick={onSubmit}
                        isLoading={isLoading}
                    >
                        Kirim
                    </PrimaryButton>
                </Flex>
            </Flex>
        </>
    )
}

export default ContactForm;