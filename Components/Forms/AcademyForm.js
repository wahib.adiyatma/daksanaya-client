import { Grid } from "@chakra-ui/layout";
import { ErrorToast, SuccessToast } from '../Toast';
import {
    FormControl,
    FormLabel,
} from "@chakra-ui/react"
import { Input } from "@chakra-ui/input";
import { academyFormApi } from "../../_api/service";
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { VStack } from '@chakra-ui/layout';
import PrimaryButton from '../Button/PrimaryButton'
import { toastErrorMessage } from '../../utils/functions';
import { REGEX_EMAIL } from '../../constants/regex';
import { Textarea } from "@chakra-ui/textarea";

const AcademyForm = () => {
    const {
        register,
        getValues,
        trigger,
        formState: { errors },
    } = useForm();
    const validate = {
        required: { value: true, message: 'Silakan isi seluruh field' },
    };

    const [isLoading, setIsLoading] = useState(false);

    const onSubmit = async (e) => {
        e?.preventDefault();

        if (!(await trigger())) {
            toastErrorMessage({ errors, title: 'Gagal mengirim pesan' });
            return;
        }

        setIsLoading(true);
        academyFormApi
            .addAcademyForm({ ...getValues() })
            .then(() => SuccessToast('Pesan berhasil dikirim'))
            .catch((err) => ErrorToast('Gagal mengirim pesan'))
            .finally(() => setIsLoading(false));
    };
    return (
        <VStack spacing={8}>
            <FormControl isRequired>
                <FormLabel>Nama Anda</FormLabel>
                <Input
                    {...register('name', { ...validate })}
                />
            </FormControl>
            <FormControl isRequired>
                <FormLabel>Nama Instansi/Perusahaan</FormLabel>
                <Input
                    {...register('company', { ...validate })}
                />
            </FormControl>
            <FormControl isRequired>
                <FormLabel>Alamat Email</FormLabel>
                <Input
                    {...register('email', {
                        ...validate,
                        pattern: {
                            value: REGEX_EMAIL,
                            message: 'Email tidak valid',
                        },
                    })}
                />
            </FormControl>
            <FormControl >
                <FormLabel>Nomor Telepon</FormLabel>
                <Input
                    {...register('phoneNumber', { ...validate })}
                />
            </FormControl>
            <FormControl >
                <FormLabel>Bantuan Yang Diperlukan</FormLabel>
                <Textarea
                    h="150px"
                    {...register('message', { ...validate })}
                />
            </FormControl>
            <PrimaryButton
                my="40px"
                mt={4}
                h="50px"
                type="submit"
                maxW={{ lg: "15vw", base: '100vw' }}
                ml={{ lg: "33vw" }}
                onClick={onSubmit}
                isLoading={isLoading}
            >
                Daftar Sekarang
            </PrimaryButton>
        </VStack>
    )
}
export default AcademyForm;