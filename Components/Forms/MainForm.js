import { ErrorToast, SuccessToast } from '../Toast';
import {
    FormControl,
    FormLabel,
    Textarea
} from "@chakra-ui/react"
import { Input } from "@chakra-ui/input";
import { consultationApi } from '../../_api/service';
import { useForm } from 'react-hook-form';
import { useEffect, useState } from 'react';
import { Flex, VStack, Text } from '@chakra-ui/layout';
import { Image } from '@chakra-ui/image';
import PrimaryButton from '../Button/PrimaryButton'
import { toastErrorMessage } from '../../utils/functions';

const MainForm = () => {
    const {
        register,
        getValues,
        trigger,
        formState: { errors },
    } = useForm();
    const validate = {
        required: { value: true, message: 'Silakan isi seluruh field' },
    };

    const [isLoading, setIsLoading] = useState(false);

    const onSubmit = async (e) => {
        e?.preventDefault();

        if (!(await trigger())) {
            toastErrorMessage({ errors, title: 'Gagal mengirim pesan' });
            return;
        }

        setIsLoading(true);
        consultationApi
            .addConsultation({ ...getValues() })
            .then(() => SuccessToast('Pesan berhasil dikirim'))
            .catch((err) => ErrorToast('Gagal mengirim pesan'))
            .finally(() => setIsLoading(false));
    };
    return (
        <Flex
            flexDir={{ lg: 'row', base: 'column-reverse' }}
            flexWrap="wrap"
            justifyContent="center"
            alignItems="center"
            w="100vw"
            mt={{ lg: "-80px" }}
            mb="40px"
            mx="auto"

        >
            <Flex
                flexDir='column'
                flexWrap="wrap"
                w={{ lg: "550px", base: "350px" }}
                my={{ lg: '40px', base: '40px' }}

            >
                <Text
                    fontSize={{ lg: '1.5rem', base: '1.2rem' }}
                    fontWeight="semibold"
                    maxW="90vw"
                    my={{ lg: "10px" }}
                >
                    Apakah anda ingin berkomunikasi dan berkonsultasi lebih lanjut bersama kami ?
                </Text>
                <Text
                    my={{ lg: "10px", base: '20px' }}
                >
                    Lengkapi data diri Anda dan kami akan segera menghubungi Anda.
                </Text>
                <VStack spacing={8}>
                    <FormControl id="name" isRequired>
                        <FormLabel>Nama Anda</FormLabel>
                        <Input placeholder="Contoh : Wahib Adiyatma" {...register('name', { ...validate })} />
                    </FormControl>
                    <FormControl id="contact">
                        <FormLabel>Nomor Telepon</FormLabel>
                        <Input placeholder="Contoh : 081227363388" {...register('phoneNumber', { ...validate })} />
                    </FormControl>
                    <FormControl id="help">
                        <FormLabel>Bantuan Yang Diperlukan</FormLabel>
                        <Textarea
                            placeholder="Contoh : Konsultasi untuk dana pensiun"
                            {...register('message', { ...validate })}
                            h={{ lg: '150px' }}
                        />
                    </FormControl>
                    <PrimaryButton
                        mt={4}
                        type="submit"
                        maxW={{ lg: "10vw", base: '100vw' }}
                        ml={{ lg: "33vw" }}
                        onClick={onSubmit}
                        isLoading={isLoading}
                    >
                        Kirim
                    </PrimaryButton>
                </VStack>

            </Flex>
            <Image
                src="/images/konsultasi-keuangan/consulting-img-1.svg"
                ml={{ lg: "50px", base: '0px' }}
                w={{ lg: "500px", base: '300px' }}
                h={{ lg: "500px", base: '300px' }}
            />
        </Flex>
    )
}

export default MainForm;