import { Box, Flex, Text, Grid, Link, Image, VStack, HStack } from '@chakra-ui/react';
import { paddingContainer } from '.';


const socialMediaDaksa = [
    { id: 'sm1', name: 'linkedin', href: 'https://www.linkedin.com/company/daksanaya/' },
    {
        id: 'sm2',
        name: 'instagram',
        href: 'https://www.instagram.com/daksanaya/',
    },
    {
        id: 'sm3',
        name: 'youtube',
        href: 'https://www.youtube.com/channel/UCHznTueEowuC0u2aBdPP_Gw',
    },
    { id: 'sm4', name: 'facebook', href: 'https://www.facebook.com/daksanaya' },
];
const kontakDaksa = [
    { id: 'kt1', name: 'whatsapp', href: 'https://api.whatsapp.com/send/?phone=081299098344&text&app_absent=0', text: "+62 812-9909-8344" },
    { id: 'kt2', name: 'gmail', href: '#', text: "daksanayamanagement@gmail.com" }
]

const Footer = () => (

    <Flex
        as="footer"
        px={paddingContainer}
        align={{ base: 'left', lg: 'center' }}
        justify={{ base: 'center', md: 'space-between' }}
        flexDirection={{ base: 'column', md: 'row' }}
        h={{ base: '400px', lg: '230px' }}
        minWidth="100vw"
        bg="primary.darkBlueDaksa"
        color="white"
    >
        <Box
            textAlign="left"
            my={{ base: '12px', lg: '0px' }}

        >
            <Text
                fontSize={{ base: '1.25rem', lg: '1.5rem' }}
                fontWeight="bold"
                mb={{ base: '8px', lg: '12px' }}

            >PT. Daksanaya Manajemen</Text>
            <Text
                maxW={{ base: '60vw', lg: '30vw' }}
                fontSize={{ base: '1rem', lg: '1.25rem' }}
                fontWeight="medium"
            >
                Jl. Mampang Prapatan Raya No. 73 Jakarta Selatan, DKI Jakarta, Indonesia
            </Text>
        </Box>
        <VStack
            align="left"
            my={{ base: '12px', lg: '0px' }}
        >
            <Text
                fontSize={{ base: '1.25rem', lg: '1.5rem' }}
                fontWeight="bold"
                mb={{ base: '8px', lg: '12px' }}
            >
                Sosial Media
            </Text>
            <HStack
                spacing={{ base: '24px', lg: '26px' }}
            >
                {socialMediaDaksa.map(({ id, name, href }) => (
                    <Link key={id} href={href} target="_blank">
                        <Image
                            alt={name}
                            src={`/images/${name}.svg`}
                            w={{ base: '30px', md: '35px', lg: '40px' }}
                            h={{ base: '30px', md: '35px', lg: '40px' }}
                        />
                    </Link>
                ))}
            </HStack>
        </VStack>
        <VStack
            align="left"
            my={{ base: '12px', lg: '0px' }}
        >
            <Text
                fontSize={{ base: '1.25rem', lg: '1.5rem' }}
                fontWeight="bold"
                mb={{ base: '8px', lg: '12px' }}
            >
                Kontak Kami
            </Text>
            {kontakDaksa.map(({ id, name, href, text }) => (
                <Link key={id} href={href} target="_blank">
                    <HStack>
                        <Image
                            alt={name}
                            src={`/images/${name}.svg`}
                            w={{ base: '30px', md: '35px', lg: '40px' }}
                            h={{ base: '30px', md: '35px', lg: '40px' }}
                        />
                        <Text>{text}</Text>
                    </HStack>
                </Link>
            ))}
        </VStack>
    </Flex>
);

export default Footer;
