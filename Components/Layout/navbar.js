/* eslint-disable @next/next/no-img-element */
import {
    Box,
    Flex,
    Text,
    IconButton,
    Button,
    Stack,
    Link,
    Collapse,
    Icon,
    Menu,
    MenuButton,
    MenuList,
    useColorModeValue,
    useDisclosure,
} from '@chakra-ui/react';
import { HamburgerIcon, CloseIcon, ChevronDownIcon, ChevronRightIcon } from '@chakra-ui/icons';
import { useRouter } from 'next/dist/client/router';


export const navbarMenu = {
    Beranda: "/beranda",
    Profil: "/profil",
    Layanan: "/layanan",
    Blog: "/blog",
    'Hubungi Kami': "/hubungi-kami",

}

const NAV_ITEMS = [
    { label: 'Beranda', href: '/' },
    { label: 'Profil', href: '/profil' },
    {
        label: 'Layanan',
        children: [
            {
                label: 'Akademi',
                href: '/akademi',
                // children: [
                //     { label: 'Perencanaan Keuangan', href: '/akademi/perencanaan-keuangan' },
                //     { label: 'Keuangan Perusahaan', href: '/akademi/keuangan-perusahaan' },
                //     { label: 'Manajemen Investasi', href: '/akademi/manajemen-investasi' },
                //     { label: 'Analisis Laporan Keuangan', href: '/akademi/analisi-laporan-keuangan' },
                //     { label: 'Valuasi Saham', href: '/akademi/valuasi-saham' }
                // ]
            },
            { label: 'Konsultasi Keuangan', href: '/konsultasi-keuangan' },
            { label: 'Riset', href: '/riset' },
        ]
    },
    { label: 'Blog', href: '/blog' },
    { label: 'Hubungi Kami', href: '/hubungi-kami' },
];

const Navbar = () => {

    const { isOpen, onToggle } = useDisclosure();

    return (
        <Box
            position='fixed'
            top={0}
            zIndex='999'

        >
            <Flex
                bg='primary.whiteDaksa'
                color='primary.darkBlueDaksa'
                minWidth='100vw'
                py={{ lg: 4 }}
                px={{ base: 4, lg: 8 }}
                align={'center'}
                justify='space-between'
                filter='drop-shadow(0px 5px 20px rgba(55, 84, 170, 0.25))'
                backgroundColor='rgba(255, 255, 255, 0.699)'
                backdropFilter='blur(8px)'
                boxShadow='rgba(46, 50, 70, 0.2) 0px 1px 10px'
                transition='background-color 1s ease 0s'
                height="80px"
            >
                <Flex
                    flex={{ base: 4, md: 10, lg: 6 }}
                    justify={{ md: 'start' }}
                >
                    <img
                        src="/images/daksanaya-logo.png"
                        alt="logo"
                        href='/'
                        objectFit='contain'
                        width="120px"
                        height="60px"
                    />
                </Flex>

                <Stack
                    flex={{ base: 1, md: 1 }}
                    justify={'end'}
                    direction={'row'}
                    align='center'
                    spacing={8}
                >
                    <Flex display={{ base: 'none', lg: 'flex' }} >
                        <DesktopNav />
                    </Flex>

                </Stack>
                <Flex
                    flex={{ base: 1, md: 'auto' }}
                    display={{ base: 'flex', lg: 'none' }}
                    justify='flex-end'
                >
                    <IconButton
                        onClick={onToggle}
                        icon={
                            isOpen ? <CloseIcon w={4} h={4} /> : <HamburgerIcon w={6} h={6} />
                        }
                        variant={'ghost'}
                        aria-label={'Toggle Navigation'}
                    />
                </Flex>
            </Flex>
            <Collapse in={isOpen} animateOpacity>
                <MobileNav />
            </Collapse>

        </Box >
    );
}

const DesktopNav = () => {
    const route = useRouter();

    return (
        <Stack direction={'row'} spacing={3}>
            {NAV_ITEMS.map((navItem) => (
                <Box key={navItem.label}>

                    <Menu placement='auto'>
                        <MenuButton
                            as={Button}
                            variant='unstyled'
                            rightIcon={navItem.children ? <ChevronDownIcon boxSize={3} /> : ''}
                            mx="10px"
                            fontSize={{ md: '16px', lg: "18px" }}
                            fontWeight={400}
                            href={navItem.href ?? '#'}
                            onClick={() => navItem.href ? route.push(navItem.href) : null}
                            _hover={
                                !navItem.children ? {
                                    ':after': { width: '40px' }
                                } : ''
                            }
                            _active={
                                !navItem.children ? {
                                    ':after': { width: '40px' },
                                    fontWeight: '600',
                                }
                                    : ''
                            }
                            _focus={{}}
                            _after={{
                                content: '""',
                                display: 'block',
                                height: '3.5px',
                                width: '0px',
                                position: 'absolute',
                                background: '#0e3d67',
                                borderRadius: '3px',
                                transition: 'all 0.4s'
                            }}
                        >
                            {navItem.label}
                        </MenuButton>

                        {navItem.children && (
                            <MenuList
                                minW="0"
                                w={'140px'}
                                fontSize='16px'
                            >
                                {navItem.children.map((child) => (
                                    <DesktopSubNav key={child.label} {...child} />
                                ))}
                            </MenuList>
                        )}
                    </Menu>
                </Box>
            ))}
        </Stack>
    );
};

const DesktopSubNav = ({ label, href, subLabel }) => {
    return (
        <Link
            href={href}
            role={'group'}
            display={'block'}
            p={2}
            rounded={'md'}
            _hover={{ bg: 'primary.blueGray50' }}>
            <Stack direction={'row'} align={'center'}>
                <Box>
                    <Text
                        transition={'all .3s ease'}
                        _groupHover={{ color: 'blue.400' }}
                        fontWeight={500}
                    >
                        {label}
                    </Text>
                    <Text fontSize={'sm'}>{subLabel}</Text>
                </Box>
                <Flex
                    transition={'all .3s ease'}
                    transform={'translateX(-10px)'}
                    opacity={0}
                    _groupHover={{ opacity: '100%', transform: 'translateX(0)' }}
                    justify={'flex-end'}
                    align={'center'}
                    flex={1}>
                    <Icon color={'blue.400'} w={5} h={5} as={ChevronRightIcon} />
                </Flex>
            </Stack>
        </Link>
    );
};

const MobileNav = () => {

    return (
        <Stack
            bg='white'
            p={4}
            fontSize='15px'
            display={{ lg: 'none' }}
        >
            {NAV_ITEMS.map((navItem) => (
                <MobileNavItem key={navItem.label} {...navItem} />
            ))}
        </Stack>
    );
};

const MobileNavItem = ({ label, children, href }) => {
    const { isOpen, onToggle } = useDisclosure();

    return (
        <Stack spacing={2} onClick={children && onToggle}>
            <Flex
                py={0}
                as={Link}
                href={href ?? '#'}
                justify={'space-between'}
                align={'center'}
                _hover={{
                    textDecoration: 'none',
                    bg: 'primary.grayFuki',
                    transition: 'all .3s ease',
                }}
            >
                <Text
                    display='block'
                    width='100%'
                    height='100%'
                    pt={1}
                    pb={1}
                    fontWeight={500}
                    color='primary.darkBlue'
                    _hover={{
                        color: 'blue.400',
                        fontWeight: '600'
                    }}
                >
                    {label}
                </Text>

                {children && (
                    <Icon
                        as={ChevronDownIcon}
                        transition={'all .25s ease-in-out'}
                        transform={isOpen ? 'rotate(180deg)' : ''}
                        w={6}
                        h={6}
                    />
                )}
            </Flex>

            <Collapse in={isOpen} animateOpacity style={{ marginTop: '0!important' }}>
                <Stack
                    mt={2}
                    pl={4}
                    borderLeft={1}
                    borderStyle={'solid'}
                    borderColor={useColorModeValue('gray.200', 'gray.700')}
                    align={'start'}
                >
                    {children &&
                        children.map((child) => (
                            <Link
                                display='block'
                                width='100%'
                                key={child.label}
                                py={0}
                                href={child.href}
                                _hover={{
                                    outline: 'none',
                                    color: 'blue.400',
                                    fontWeight: '600',
                                }}
                            >
                                {child.label}
                            </Link>
                        ))}
                </Stack>
            </Collapse>
        </Stack>
    );
};

export default Navbar;
