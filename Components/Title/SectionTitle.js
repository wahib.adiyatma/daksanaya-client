import { Text, Box } from "@chakra-ui/layout"

const SectionTitle = ({ children, ...props }) => {
    return (
        <>
            <Text
                borderBottom="6px solid #0E3D67"
                fontSize={{ lg: "1.75rem", base: '1.25rem' }}
                fontWeight="semibold"
                {...props}
            >
                {children}
            </Text>
        </>

    )
}

export default SectionTitle;