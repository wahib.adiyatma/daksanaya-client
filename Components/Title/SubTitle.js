import { Text } from "@chakra-ui/layout";

const SubTitle = ({ children, ...props }) => {
    return (
        < Text
            fontWeight="bold"
            align="center"
            fontSize={{ base: "1.5rem", lg: "3rem" }}
            {...props}
        >
            {children}
        </Text >
    );
}
export default SubTitle;