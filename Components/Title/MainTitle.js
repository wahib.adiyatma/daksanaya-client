import { Text } from '@chakra-ui/react';

const MainTitle = ({ children, ...props }) => {
    return (
        <Text
            bgClip="text"
            fontSize={{ base: '2rem', lg: '4rem' }}
            fontWeight="extrabold"
            {...props}
        >
            {children}
        </Text>
    );
};

export default MainTitle;
