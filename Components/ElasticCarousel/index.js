import { Box } from '@chakra-ui/layout';
import React from 'react';
import Carousel from 'react-elastic-carousel';
import { galleriesApi } from '../../_api/service';
import { useEffect, useState } from 'react';
import { Skeleton } from "@chakra-ui/react"
import config from "../../_api/config"

import CarouselItem from './Item';

const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2, itemsToScroll: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 4 }
];

function ElasticCarousel() {

    const [galleries, setGalleries] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        galleriesApi.getAllGalleries().then((data) => {
            setIsLoaded(true);
            setGalleries(data);
        });
    }, []);

    return (
        <Box>
            <Carousel
                breakPoints={breakPoints}
                enableSwipe={true}
                enableAutoPlay={true}
                autoPlaySpeed={4000}
                pagination={false}
                showArrows={false}
            >
                {galleries.map((gallery) => (
                    <Skeleton isLoaded={isLoaded}>
                        <CarouselItem
                            key={gallery.slug}
                            title={gallery.title}
                            desc={gallery.desc}
                            imgSrc={`${config.STORAGE_BASE_URL}${gallery.image.url}`}
                        />
                    </Skeleton>

                ))}
            </Carousel>
        </Box>
    );
}

export default ElasticCarousel;
