import { Image } from '@chakra-ui/image';

const CarouselItem = ({ title, imgSrc, desc }) => {
    return (
        <Image
            src={imgSrc}
            justifyContent="center"
            alignItems="center"
            height='300px'
            width="100vw"
            margin="15px"
        ></Image>
    );
};
export default CarouselItem;
