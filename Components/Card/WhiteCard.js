import { Flex } from '@chakra-ui/react';

const WhiteCard = ({ children, ...props }) => {
    return (
        <Flex
            borderRadius="30px"
            boxShadow="-4px -4px 4px rgba(80, 139, 255, 0.08), 0px 4px 4px rgba(255, 255, 255, 0.04), 15px 15px 30px rgba(80, 139, 255, 0.1), inset 4px 4px 20px rgba(255, 255, 255, 0.5)"
            backgroundColor="white"

            {...props}
        >
            {children}
        </Flex>
    );
}

export default WhiteCard;