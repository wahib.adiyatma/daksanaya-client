import { Box } from '@chakra-ui/react';

export const BlackCard = ({ children, ...props }) => {
    return (
        <Box
            borderRadius="30px"
            boxShadow="15px 15px 30px rgba(20, 27, 36, 0.5), -15px -15px 30px rgba(255, 255, 255, 0.25)"
            backgroundColor="primary.blackDaksa2"
            {...props}
        >
            {children}
        </Box >
    );
}
export default BlackCard;
