import { Box } from '@chakra-ui/react';

const Background = ({ children, ...props }) => {
    return (
        <Box
            w="100%"
            {...props}
        >
            {children}
        </Box>
    );
}
export default Background;