import styled from "styled-components";

export const MainWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-image: url("/images/hubungi-kami/header.png");
    background-size: cover;
    background-repeat: no-repeat;
    min-width: 100vw;
    min-height:400px;   
`

export const SectionGap = styled.div`
  padding-top: 120px;
  display: flex;
  flex-direction: column;
  align-items: center;
`