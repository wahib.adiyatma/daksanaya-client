import MainTitle from "../../Title/MainTitle";
import { MainWrapper } from "./Style";
import { Box, Flex, HStack, VStack, Text } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import ContactForm from "../../Forms/ContactForm";


const HubungiKami = ({ }) => {
    return (
        <>
            <MainWrapper>
                <Box
                    display="flex"
                    flexDir="column"
                    justifyContent="center"
                    alignItems="center"
                >
                    <MainTitle
                        color="white"
                    >
                        Hubungi Kami
                    </MainTitle>
                </Box>
            </MainWrapper>
            <Flex
                w="80vw"
                h={{ lg: "500px", base: "800px" }}
                my="100px"
                mx="auto"
                borderRadius="50px"
                boxShadow="15px 15px 10px rgba(0, 0, 0, 0.1), -15px -15px 10px rgba(255, 255, 255, 0.1)"
                py="50px"
                flexDir="row"
                flexWrap="wrap"
                justifyContent="center"
                alignItems="cennter"
            >
                <Flex
                    flexDir="column"
                    justifyContent="center"
                    alignItems="left"
                    mx={{ lg: "40px" }}
                >
                    <HStack
                        my={{ lg: "30px", base: "15px" }}
                    >
                        <Image
                            src="/images/icons/map.png"
                            maxW="35px"
                            maxH="35px"
                            mr="30px"
                        />
                        <VStack align="left" maxW="250px">
                            <Text fontSize="1rem" fontWeight="medium">Lokasi :</Text>
                            <Text>Jl. Mampang Prapatan Raya No. 73 Jakarta Selatan</Text>

                        </VStack>
                    </HStack>

                    <HStack
                        my={{ lg: "30px", base: "15px" }}
                    >
                        <Image
                            src="/images/icons/gmail.png"
                            maxW="35px"
                            maxH="35px"
                            mr="30px"
                        />
                        <VStack align="left" maxW="250px">
                            <Text fontSize="1rem" fontWeight="medium">
                                Email :
                            </Text>
                            <Text>daksanayamanagement@gmail.com</Text>

                        </VStack>
                    </HStack>

                    <HStack
                        my={{ lg: "30px", base: "15px" }}
                    >
                        <Image
                            src="/images/icons/call.png"
                            maxW="35px"
                            maxH="35px"
                            mr="30px"
                        />
                        <VStack align="left" maxW="250px">
                            <Text fontSize="1rem" fontWeight="medium">
                                Telepon :
                            </Text>
                            <Text>+62 812-9909-8344</Text>
                        </VStack>
                    </HStack>
                </Flex>
                <ContactForm />
            </Flex>


        </>
    )
}

export default HubungiKami;