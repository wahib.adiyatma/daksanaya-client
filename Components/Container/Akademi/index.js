import { Box, Text, Center, Flex } from "@chakra-ui/layout";
import { MainWrapper, SectionGap } from "./Style";
import MainTitle from "../../Title/MainTitle";
import WhiteCard from "../../Card/WhiteCard";
import { VStack } from "@chakra-ui/react";
import FiturBox from "./FiturBox";
import SectionTitle from '../../Title/SectionTitle'
import "semantic-ui-css/semantic.min.css";
import "react-multi-carousel/lib/styles.css";
import MultiCarousel from "./MultiCarousel";


const Akademi = ({ }) => {
    return (
        <>
            <Flex
                flexDir="column"
                justifyContent="center"
                bgImage="url(/images/akademi/header.jpeg)"
                backgroundSize="cover"
                backgroundRepeat="no-repeat"
                minW="100vw"
                minH="500px"
                position="relative"
            >
                <Box
                    backgroundColor="rgba(0, 0, 0, 0.6)"
                    position="absolute"
                    top="0"
                    left="0"
                    width="100%"
                    height="100%"
                >
                    <Box
                        display="flex"
                        flexDir="column"
                        justifyContent="center"
                        alignItems="center"
                        pt="200px"
                    >
                        <MainTitle
                            color="white"
                        >
                            Akademi
                        </MainTitle>

                    </Box>
                </Box>
            </Flex >
            <WhiteCard
                mx="auto"
                h={{ lg: "350px", base: "550px" }}
                w={{ lg: "60vw", base: '90vw' }}
                border="1px solid #C4C4C4"
                mb="30px"
                mt="60px"
            >
                <VStack mx="auto" spacing="40px" maxW={{ lg: "50vw", base: '75vw' }}>
                    <Text
                        pt="30px"
                        fontSize={{ lg: "2rem", base: "1.5rem" }}
                    >
                        Deskripsi Kegiatan
                    </Text>
                    <Text
                        fontSize={{ lg: "1rem", base: "1rem" }}
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget posuere justo. Vestibulum ut feugiat nunc. Duis sed gravida eros, consequat dignissim arcu. Sed lorem risus, vehicula sed egestas at, venenatis ut lectus. Nulla accumsan ipsum et quam feugiat viverra. Duis quis lectus arcu. Nullam mollis lorem eu justo consectetur venenatis.
                    </Text>
                </VStack>


            </WhiteCard>

            <Center>
                <SectionTitle
                >
                    Menu Materi
                </SectionTitle>
            </Center>

            <FiturBox />

            <Box
                w="100vw"
                h={{ lg: "600px", base: '400px' }}
                bgColor="primary.darkBlueDaksa"
                borderRadius={{ lg: "150px 150px 0px 0px", base: '50px 50px 0px 0px' }}
            >
                <MultiCarousel
                    pt={{ lg: "175px", base: '50px' }}
                />
            </Box>

        </>
    )
}

export default Akademi;



