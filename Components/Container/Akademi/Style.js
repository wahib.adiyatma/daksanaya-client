import styled from "styled-components";

export const MainWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-image: url("/images/akademi/header.jpeg"); 
    background-size: cover;
    background-repeat: no-repeat;
    min-width: 100vw;
    min-height:500px;   
    position: relative;
`

export const ServiceWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-image: url("/images/akademi/perencanaan-keuangan.jpeg"); 
    background-size: cover;
    background-repeat: no-repeat;
    min-width: 100vw;
    min-height:500px;   
    position: relative;
`

export const SectionGap = styled.div`
  padding-top: 120px;
  display: flex;
  flex-direction: column;
  align-items: center;
`