import { Box, Text, Flex, Spacer } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import { Grid, GridItem } from "@chakra-ui/react"
import { ChevronRightIcon } from '@chakra-ui/icons'
import { Button } from "@chakra-ui/button";
import { useRouter } from "next/dist/client/router";

const FITUR_ITEMS = [
    { label: 'Perencanaan Keuangan', slug: 'perencanaan-keuangan', href: '/akademi/perencanaan-keuangan' },
    { label: 'Keuangan Perushaan', slug: 'keuangan-perusahaan', href: '/akademi/keuangan-perusahaan' },
    { label: 'Manajemen Investasi', slug: 'manajemen-investasi', href: '/akademi/manajemen-investasi' },
    { label: 'Analisis Laporan Keuangan', slug: 'analisis-laporan-keuangan', href: '/akademi/analisis-laporan-keuangan' },
    { label: 'Valuasi Saham', slug: 'valuasi-saham', href: '/akademi/valuasi-saham' }
];

const FiturBox = () => {
    const route = useRouter();
    return (
        <>
            <Flex
                display="flex"
                flexDir={{ lg: 'row', base: 'column-reverse' }}
                justifyContent="center"
                alignItems="center"
                h={{ lg: "500px", base: "800px" }}

            >
                <Flex
                    flexDir='column'
                    justifyContent="center"
                    alignItems='center'
                    maxW="40vw"
                    mx="20px"

                >
                    {FITUR_ITEMS.map((fiturItem) => (
                        <Button
                            key={fiturItem.slug}
                            h="50px"
                            w={{ lg: "35vw", base: "80vw" }}
                            borderRadius="10px"
                            backgroundColor="white"
                            boxShadow="-4px -4px 4px rgba(80, 139, 255, 0.08), 0px 4px 4px rgba(255, 255, 255, 0.04), 15px 15px 30px rgba(80, 139, 255, 0.1), inset 4px 4px 20px rgba(255, 255, 255, 0.5)"
                            my="10px"
                            onClick={() => fiturItem.href ? route.push(fiturItem.href) : null}
                        >
                            <Flex
                                flexDir="row"
                                flexWrap="wrap"
                                alignItems="center"
                                justifyContent="center"
                            >

                                <Image
                                    src={`images/icons/${fiturItem.slug}.svg`}
                                    mx="auto"
                                />
                                <Spacer />
                                <Text align="left" mx={{ lg: '20px', base: '10px' }} w={{ lg: "20vw", base: '40vw' }} fontSize={{ lg: '1rem', base: '0.75rem' }}>{fiturItem.label}</Text>
                                <Spacer />
                                <ChevronRightIcon ml={2} w={6} h={6} />
                            </Flex>
                        </Button>

                    ))}
                </Flex>
                <Flex
                    flexDir='column'
                    justifyContent="left"
                    alignItems='left'
                    maxW={{ lg: "30vw", base: '75vw' }}
                    mx={{ lg: "50px" }}
                    mt={{ base: '3rem', lg: '-1rem' }}
                >
                    <Text
                        fontSize={{ lg: "2rem", base: "1.5rem" }}
                        mb="30px"

                    >
                        Materi yang kami tawarkan
                    </Text>
                    <Text
                        fontSize={{ lg: "1.25rem", base: '1rem' }}
                        mb={{ base: '2rem' }}
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </Text>
                </Flex>
            </Flex>
        </>
    )
}

export default FiturBox;