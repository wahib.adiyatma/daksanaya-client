export const SERVICE_OPTIONS = [

    [
        { id: 0 },
        { title: "Perencanaan Keuangan" },
        { deskripsi: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget posuere justo. Vestibulum ut feugiat nunc. Duis sed gravida eros, consequat dignissim arcu. Sed lorem risus, vehicula sed egestas at, venenatis ut lectus. Nulla accumsan ipsum et quam feugiat viverra. Duis quis lectus arcu. Nullam mollis lorem eu justo consectetur venenatis." },
        { offer: ["Overview perencanaan keuangan", "Mengatur likuiditas", "Simulasi dan case study", "Strategi investasi (aset riil dan aset keuangan)", "Menghitung arus kas", "Mengatur resiko", "Menyiapkan dana pensiun dan warisan"] },
        { benefit: ["Softcopy materi", " E-Sertifikat", "Rekaman materi", "Relasi"] }
    ],

    [
        { id: 1 },
        { title: "Keuangan Perusahaan" },
        { deskripsi: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget posuere justo. Vestibulum ut feugiat nunc. Duis sed gravida eros, consequat dignissim arcu. Sed lorem risus, vehicula sed egestas at, venenatis ut lectus. Nulla accumsan ipsum et quam feugiat viverra. Duis quis lectus arcu. Nullam mollis lorem eu justo consectetur venenatis." },
        { offer: ["Overview perencanaan keuangan", "Mengatur likuiditas", "Simulasi dan case study", "Strategi investasi (aset riil dan aset keuangan)", "Menghitung arus kas", "Mengatur resiko", "Menyiapkan dana pensiun dan warisan"] },
        { benefit: ["Satu buah e-book keuangan perusahaan", "Softcopy materi", " E-Sertifikat", "Rekaman materi", "Relasi"] }
    ],

    [
        { id: 2 },
        { title: "Manajemen Investasi" },
        { deskripsi: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget posuere justo. Vestibulum ut feugiat nunc. Duis sed gravida eros, consequat dignissim arcu. Sed lorem risus, vehicula sed egestas at, venenatis ut lectus. Nulla accumsan ipsum et quam feugiat viverra. Duis quis lectus arcu. Nullam mollis lorem eu justo consectetur venenatis." },
        { offer: ["Overview perencanaan keuangan", "Mengatur likuiditas", "Simulasi dan case study", "Strategi investasi (aset riil dan aset keuangan)", "Menghitung arus kas", "Mengatur resiko", "Menyiapkan dana pensiun dan warisan"] },
        { benefit: ["Softcopy materi", " E-Sertifikat", "Rekaman materi", "Relasi"] }
    ],

    [
        { id: 3 },
        { title: "Analisis Laporan Keuangan" },
        { deskripsi: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget posuere justo. Vestibulum ut feugiat nunc. Duis sed gravida eros, consequat dignissim arcu. Sed lorem risus, vehicula sed egestas at, venenatis ut lectus. Nulla accumsan ipsum et quam feugiat viverra. Duis quis lectus arcu. Nullam mollis lorem eu justo consectetur venenatis." },
        { offer: ["Overview laporan keuangan", "Laporan laba-rugi, neraca, dan arus kas", "Analisa rasio-rasio penting", "Analisa nilai tambah perusahaan", "Penggunaan laporan keuangan", "Analisa horizontal dan vertikal", "Analisa kebangkrutan", "Studi Kasus"] },
        { benefit: ["Softcopy materi", " E-Sertifikat", "Rekaman materi", "Relasi"] }
    ],
    [
        { id: 4 },
        { title: "Valuasi Saham" },
        { deskripsi: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies urna a arcu ultricies molestie. Phasellus vestibulum, sem tristique bibendum pellentesque, urna elit feugiat lorem, ut finibus nulla odio sit amet diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget posuere justo. Vestibulum ut feugiat nunc. Duis sed gravida eros, consequat dignissim arcu. Sed lorem risus, vehicula sed egestas at, venenatis ut lectus. Nulla accumsan ipsum et quam feugiat viverra. Duis quis lectus arcu. Nullam mollis lorem eu justo consectetur venenatis." },
        { offer: ["Overview penilaian saham", "Pendekatan harga perusahaan", "Studi Kasus", "Pendekatan Perusahaan dengan relative approach", "Prinsip dasar (filosofi) stock valuation", "Scratch pad stock valuation", "Pendekatan discounted cashflow"] },
        { benefit: ["Softcopy materi", " E-Sertifikat", "Rekaman materi", "Relasi"] }
    ]
]