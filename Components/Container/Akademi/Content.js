import React from 'react';
import { Box, Text, VStack } from '@chakra-ui/react';
import PrimaryButton from '../../Button/PrimaryButton'

const Content = ({ }) => {
    return (
        <>
            <Box
                w={{ lg: "480px", base: '300px' }}
                h={{ lg: "380px", base: '260px' }}
                borderRadius="40px"
                color="white"
                bgImage=" https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
            >

            </Box>
        </>
    );
}

export default Content;
