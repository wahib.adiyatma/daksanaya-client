import styled from "styled-components";

export const MainWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-image: url("/images/beranda/header.png");
    background-size: cover;
    background-repeat: no-repeat;
    min-width: 100vw;
    min-height:765px;   
`

export const SectionGap = styled.div`
  padding-top: 120px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const CarouselStyle = styled.div`
@media screen and(min - width: 1024px) {
    .control - dots {

  }
}
  .carousel.control - prev: hover,
  .carousel.control - next: hover {
  background: transparent;
}
  .carousel.control - prev:: before,
  .carousel.control - next:: before {
  content: url(/chevron-arrow.svg) !important;
    border: none!important;
}
  .carousel.control - next:: before {
  transform: rotate(180deg);
}
  .carousel - status {
  display: none;
}
  .slider - wrapper {
  padding - bottom: 3rem;
}

@media screen and(max - width: 1024px) {
    .carousel.control - prev:: before {
    border - right: 12px solid #FFFFFF;

  }
    .carousel.control - next:: before {
    border - left: 12px solid #FFFFFF;

  }
    .carousel.control - dots.dot {
    background - color: #FFFFFF;
  }
}
`
export default CarouselStyle;