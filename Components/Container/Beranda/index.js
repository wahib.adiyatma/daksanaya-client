import "semantic-ui-css/semantic.min.css";
import "react-multi-carousel/lib/styles.css";
import MainTitle from '../../Title/MainTitle'
import { SectionGap } from "./Style";
import { Flex } from "@chakra-ui/layout";
import ServiceCard from "./ServiceCard";
import Carousel from "./Carousel"
import SubTitle from "../../Title/SubTitle";
import {
    Box,
    Text,
    VStack,
} from '@chakra-ui/react';
import PrimaryButton from '../../Button/PrimaryButton'
import SectionHeader from "./SectionHeader";
import EventCard from "./EventCard";
import { Image } from "@chakra-ui/image";
import MainForm from '../../Forms/MainForm'
import ElasticCarousel from "../../ElasticCarousel";


const Beranda = ({ children, ...props }) => {
    return (
        <>
            <Flex
                flexDir="row"
                backgroundSize="cover"
                backgroundRepeat="no-repeat"
                minW="100vw"
                minH={{ lg: "765px", base: "500px" }}
                justifyContent="space-between"
            >
                <Flex
                    flexDir="column"
                    maxW={{ lg: "55vw", base: "50vw" }}
                    justifyContent="center"
                    alignItems="left"

                    ml={{ lg: "100px", base: "30px" }}
                >
                    <Image
                        src="images/daksanaya-logo.png"
                        maxW={{ lg: "50vw", base: "50vw" }}
                        maxH={{ lg: "30vh", base: "50vh" }}
                    />
                    <Text
                        fontSize={{ lg: "2.5rem", base: "1.25rem" }}
                        mt="30px"
                    >
                        knowledge, networking, sharing experiences
                        practice with the best solutions
                    </Text>
                    <PrimaryButton
                        mt="30px"
                        maxW={{ lg: "10vw", base: "40vw" }}
                        h="40px"
                    >
                        Our Services
                    </PrimaryButton>

                </Flex>
                <Image
                    src="images/beranda/img-landing.png"
                    maxW={{ lg: "50vw", base: "50vw" }}
                    maxH={{ lg: "765px", base: "300px" }}
                    mt={{ base: "100px" }}
                />

            </Flex>
            <SectionGap>
                <SubTitle
                    color="primary.darkBlueDaksa"
                    marginBottom="60px"
                    mt={{ lg: "40px" }}
                >
                    Kenalin, Daksa. <br /> Si mentor keuangan kamu
                </SubTitle>
                <Box
                    id="layanan"
                    display="flex"
                    flexWrap="wrap"
                    zIndex="1"
                >
                    <ServiceCard
                        imgprops="images/icons/book.png"
                        title="Akademi Daksa"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. Sed id posuere eros."
                    />
                    <ServiceCard
                        imgprops="images/icons/paper.png"
                        title="Riset Daksa"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. Sed id posuere eros."
                    />
                    <ServiceCard
                        imgprops="images/icons/chat.png"
                        title="Konsultasi Daksa"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. Sed id posuere eros."
                    />
                </Box>
                <Box
                    bgColor="primary.blueDaksaBg"
                    w='100%'
                    h={{ base: '700px', lg: "960px" }}
                    mt={{ base: "1rem", lg: "-11rem" }}
                    borderRadius={{ lg: "150px 150px 0px 0px", base: '75px 75px 0px 0px' }}
                >
                    <SubTitle
                        color="white"
                        pt={{ base: '3rem', lg: '15rem' }}
                        lineHeight="72px"
                    >
                        Kata mereka tentang Daksa . . .
                    </SubTitle>
                    <Carousel
                        my={{ lg: "3vh", base: '1.5vh' }}
                    />
                </Box>
                <Box
                    bgColor="white"
                    w="100%"
                    h={{ lg: "1100px", base: '1800px' }}
                >
                    <SectionHeader>
                        <MainTitle
                            color="primary.darkBlueDaksa"
                            mx={{ lg: '10vw', base: 'auto' }}
                        >
                            Gapai <Text as="i">finanncial freedom</Text><br /> bersama kami
                        </MainTitle>
                    </SectionHeader>
                    <EventCard />
                </Box>
                <Box
                    bgColor="white"
                    h={{ lg: "750px", base: '550px' }}
                    w="100vw"
                    borderRadius={{ lg: "150px 150px 0px 0px", base: '75px 75px 0px 0px' }}
                    mt={{ base: "-4rem", lg: "-10rem" }}
                    alignItems="center"
                >
                    <SubTitle
                        color="primary.darkBlueDaksa"
                        py={{ base: '3rem', lg: '6rem' }}
                        lineHeight="72px"
                    >
                        Dokumentasi Kegiatan Kami
                    </SubTitle>
                    <ElasticCarousel />
                </Box>
                <MainForm />

            </SectionGap>
        </>
    )
}

export default Beranda;