import React from 'react';
import { Box, Text, VStack } from '@chakra-ui/react';
import PrimaryButton from '../../Button/PrimaryButton'

const Content = ({ }) => {
    return (
        <>
            <Box
                w={{ lg: "480px", base: '360px' }}
                h={{ lg: "380px", base: '320px' }}
                borderRadius="40px"
                color="white"
                bgImage=" https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
            >
                <VStack align="left" px={{ lg: "40px", base: '30px' }} pt={{ lg: "40px", base: '20px' }} spacing={{ lg: '50px', base: '40px' }}>
                    <Text fontSize={{ lg: "2rem", base: '1.5rem' }} fontWeight="600" maxW={{ lg: "440px", base: '300px' }}>Dibimbing oleh mentor berpengalaman</Text>

                    <Text fontSize={{ lg: "1rem", base: '1rem' }} fontWeight="400" maxW={{ lg: "350px", base: '260px' }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget pellentesque turpis. Etiam at tortor lacinia, facilisis ex vitae, rhoncus leo. </Text>
                    <PrimaryButton maxW={{ lg: "10vw", base: '25vw' }}>Selengkapnya</PrimaryButton>

                </VStack>
            </Box>
        </>
    );
}

export default Content;
