import React from 'react';
import { Box, Text, VStack } from '@chakra-ui/react';
import { Image } from "@chakra-ui/image";
import { Skeleton, SkeletonText } from "@chakra-ui/react";

function Testimony({ name, imgSrc, testimony, isLoaded }) {

    return (
        <>
            <Box>
                <VStack w="100%" spacing="3vh"  >
                    <Skeleton isLoaded={isLoaded}>
                        <Image
                            borderRadius="full"
                            maxW={{ lg: "150px", base: '75px' }}
                            src={imgSrc}
                            alt={name}
                        />
                    </Skeleton>
                    <SkeletonText isLoaded={isLoaded}>
                        <Text fontSize={{ base: '1rem', lg: "2rem" }} fontWeight="600" color="white">
                            {name}
                        </Text>
                    </SkeletonText>
                    <SkeletonText isLoaded={isLoaded}>
                        <Text color="white" fontSize={{ base: '1rem', lg: "1.5rem" }} fontWeight="400" maxW="75vw" lineHeight={{ lg: "3rem", base: '2rem' }} >
                            &quot; {testimony} &quot;
                        </Text>
                    </SkeletonText>
                    <Image
                        borderRadius="full"
                        maxH="121px"
                        maxW="91px"
                        src="/images/landing-quote.png"
                        pb="8vh"
                    />

                </VStack>
            </Box>
        </>
    );
}

export default Testimony;
