import PrimaryButton from "../../Button/PrimaryButton";
import { BlackCard } from "../../Card/BlackCard";
import WhiteCard from "../../Card/WhiteCard";
import { Box, HStack, Text } from "@chakra-ui/layout";
import { Image } from "@chakra-ui/image";
import { eventApi } from '../../../_api/service';
import { useEffect, useState } from 'react';
import { Flex } from "@chakra-ui/layout";
import { Skeleton, SkeletonText } from "@chakra-ui/react"
import config from "../../../_api/config";

const EventCard = ({ children, ...props }) => {
    const [events, setEvents] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        eventApi.getAllEvents().then((data) => {
            setIsLoaded(true);
            setEvents(data);
        });
    }, []);


    const lastEvents = events.slice((events.length - 3), events.length)
    return (
        <Skeleton isLoaded={isLoaded}>
            <Flex
                flexDir={{ base: 'column', lg: 'row' }}
                flexWrap='wrap'
                alignItems="center"
                justifyContent="center"
                my="10vh"
            >
                {lastEvents.map((event) => (
                    <WhiteCard
                        w={{ lg: '350px', base: '275px' }}
                        h={{ lg: "520px", base: '440px' }}
                        display="flex"
                        flexDir="column"
                        alignItems="center"
                        justifyContent="center"
                        mx={{ lg: "30px", base: '0px' }}
                        my={{ lg: "0px", base: '30px' }}
                        key={event.slug}
                    >
                        <Skeleton isLoaded={isLoaded}>
                            <Image
                                src={`${config.STORAGE_BASE_URL}${event.image.url}`}
                                w={{ base: "250px", lg: '300px' }}
                                h={{ base: "250px", lg: '300px' }}
                                borderRadius="15px"
                                mb="15px"
                            />
                        </Skeleton>
                        <SkeletonText isLoaded={isLoaded}>
                            <Text
                                align="center"
                                color="primary.darkBlueDaksa"
                                fontSize={{ lg: "1.25rem", base: '1rem' }}
                                fontWeight="bold"
                                mb="15px"
                            >
                                {event.title}
                            </Text>
                        </SkeletonText>

                        <Box
                            alignItems="left"
                            mt="10px"
                        >
                            <HStack spacing="10px" align="left" mb="10px">
                                <Image
                                    src="/images/icons/calendar-mini.png"
                                    maxW="24px"
                                    maxH="24px"
                                />
                                <SkeletonText isLoaded={isLoaded}>
                                    <Text color="primary.darkBlueDaksa" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light">{event.date} </Text>
                                </SkeletonText>
                            </HStack>
                            <HStack spacing="10px" align="left" mb="15px">
                                <Image src="/images/icons/clock-mini.png"
                                    maxW="24px"
                                    maxH="24px"
                                />
                                <SkeletonText isLoaded={isLoaded}>
                                    <Text color="primary.darkBlueDaksa" fontSize={{ lg: "1rem", base: '0.75rem' }} fontWeight="light"> {event.timeStart} - {event.timeEnd} WIB</Text>
                                </SkeletonText>
                            </HStack>
                        </Box>
                        <PrimaryButton maxW={{ lg: "40vw", base: '30vw' }}>
                            Selengkapnya
                        </PrimaryButton>
                    </WhiteCard>
                ))};
            </Flex >
        </Skeleton>


    );
}

export default EventCard;