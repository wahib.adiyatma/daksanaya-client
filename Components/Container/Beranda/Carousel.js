import { Carousel as ReactCarousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import CarouselStyle from './Style'
import Testimony from './Testimony';
import { Box } from '@chakra-ui/layout';
import React from 'react';
import { Image } from "@chakra-ui/image";
import { useEffect, useState } from 'react';
import { testimonyApi } from '../../../_api/service';
import config from "../../../_api/config";

const Carousel = ({ ...props }) => {
    const [testimonies, setTestimonies] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        testimonyApi.getAllTestimonies().then((data) => {
            setIsLoaded(true);
            setTestimonies(data);
        });
    }, []);
    return (
        <CarouselStyle>
            <Box
                {...props}
            >
                <ReactCarousel showThumbs={false} >
                    {testimonies.map((testimony) => (
                        <Testimony
                            key={testimony.slug}
                            name={testimony.name}
                            imgSrc={`${config.STORAGE_BASE_URL}${testimony.picture.url}`}
                            testimony={testimony.testimony}
                            isLoaded={isLoaded}
                        />
                    ))}

                </ReactCarousel>
            </Box>

        </CarouselStyle>
    );

}

export default Carousel;
