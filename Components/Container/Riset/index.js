import { Flex, Box, VStack, Text } from "@chakra-ui/layout";
import { HStack, Image } from "@chakra-ui/react";
import WhiteCard from '../../Card/WhiteCard'
import MainTitle from '../../Title/MainTitle'
import SubTitle from '../../Title/SubTitle'
import BookContainer from "./BookContainer";

const Riset = ({ }) => {
    return (
        <>
            <Flex
                flexDir="column"
                justifyContent="center"
                bgImage="url(/images/riset/header.jpeg)"
                backgroundSize="cover"
                backgroundRepeat="no-repeat"
                minW="100vw"
                minH="500px"
                position="relative"
            >
                <Box
                    backgroundColor="rgba(0, 0, 0, 0.6)"
                    position="absolute"
                    top="0"
                    left="0"
                    width="100%"
                    height="100%"
                >
                    <Box
                        display="flex"
                        flexDir="column"
                        justifyContent="center"
                        alignItems="center"
                        pt={{ lg: "250px", base: '300px' }}
                    >
                        <MainTitle
                            color="white"
                        >
                            Riset dan Pengembangan
                        </MainTitle>

                    </Box>
                </Box>
            </Flex >

            <WhiteCard
                mx="auto"
                h={{ lg: "300px", base: "350px" }}
                w={{ lg: "60vw", base: '90vw' }}
                border="1px solid #C4C4C4"
                mb="30px"
                mt="60px"
            >
                <VStack mx="auto" spacing="40px" maxW={{ lg: "50vw", base: '75vw' }}>
                    <Text
                        pt="30px"
                        fontSize={{ lg: "2rem", base: "1.5rem" }}
                    >
                        Tentang Riset Daksa
                    </Text>
                    <Text
                        fontSize={{ lg: "1rem", base: "1rem" }}
                        align="center"
                    >
                        Merupakan sumber dari ilmu pengetahuan. Daksanaya memberikan edukasi dan pengetahuan melalui buku serta referensi tambahan seputar keuangan melalui buku dan jurnal sehingga dapat menambah wawasan Anda. Akses kapan pun dan dimana pun.
                    </Text>
                    {/* <Image
                        src="/images/riset/research-img-1.jpeg"
                        w={{ lg: "320px", base: '300px' }}
                        h={{ lg: "300px", base: '300px' }}
                    /> */}
                </VStack>
            </WhiteCard>

            <Flex
                w="100vw"
                h={{ lg: "1300px", base: '1900px' }}
                bgColor="primary.darkBlueDaksa"
                mt="80px"
                flexDir="column"
                justifyContent="center"
                alignItems="center"
            >
                <SubTitle
                    color="white"
                >
                    Buku dari Daksa
                </SubTitle>

                <BookContainer
                    imgSrc="/images/riset/book-1.jpeg"
                    title="Corporate Financial Management"
                    author="Dr Pardomuan Sihombing, MSM"
                    desc="Sudah banyak disajikan buku tentang manajemen keuangan dikalangan masyarakat, khususnya kalangan akademisi. Disatu sisi, hal ini menunjukkan bahwa manajemen keuangan merupakan topik yang selalu menarik untuk dibahas. Di sisi lain, setiap buku manajemen keuangan yang sudah ada di masyarakat memiliki kelebihan dan kekurangan masing-masing."

                />
                <BookContainer
                    imgSrc="/images/riset/book-2.png"
                    title="Tips Merencanakan Keuangan (Finnancial Planning)"
                    author="Dr Pardomuan Sihombing, MSM"
                    desc="Perencanaan keuangan adalah proses mencapai tujuan hidup seseorang melalui manajemen keuangansecara terintegrasi dan terencana. Cakupan Perencanaan Keuangan (dana darurat asuransi, perencanaan dana pendidikan, persiapan dana pensiun, pajak, warisan)."

                />
            </Flex>

        </>
    )
}

export default Riset;