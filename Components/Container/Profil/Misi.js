import WhiteCard from "../../Card/WhiteCard"
import { Text } from "@chakra-ui/layout"

const Misi = ({ title, desc, ...props }) => {
    return (
        <WhiteCard
            w={{ lg: "290px", base: '220px' }}
            h={{ lg: "300px", base: '250px' }}
            mx={{ lg: '30px' }}
            my={{ base: '30px' }}
            flexDir="column"
            {...props}
        >
            <Text fontSize={{ lg: "2rem", base: '1.5rem' }} fontWeight="semibold" align="center" py="5vh">
                {title}
            </Text>
            <Text fontSize={{ lg: "1.5rem", base: '1.25rem' }} fontWeight="medium" align="center" mx="auto" maxW="80%">
                {desc}
            </Text>
        </WhiteCard>
    )
}
export default Misi;