import "semantic-ui-css/semantic.min.css";
import "react-multi-carousel/lib/styles.css";
import { Box, Text, Flex } from '@chakra-ui/react';
import Header from './Header'
import SubTitle from '../../Title/SubTitle'
import BlackCard from '../../Card/BlackCard'
import WhiteCard from "../../Card/WhiteCard";
import Misi from './Misi'
import ElasticCarousel from "../../ElasticCarousel";

const Profil = ({ }) => {
    return (
        <>
            <Header>
                <Box
                    display="flex"
                    flexDir="column"
                    justifyContent="center"
                    alignItems="center"
                >
                    <SubTitle color="primary.darkBlueDaksa">Investasi untuk semua orang</SubTitle>
                    <Text fontSize={{ lg: '1.5rem', base: '1rem' }} color="primary.darkBlueDaksa" maxW="70vw" align="center">Kenal lebih dekat dengan Daksa dan orang-prang di baliknya</Text>
                </Box>
            </Header>
            <Box
                bgColor="white"
                h="1200px"
                w="100vw"
                display="flex"
                flexDir="column"
                justifyContent="center"
                alignItems="center"
            >
                <SubTitle
                    color="primary.darkBlueDaksa"
                    mb="70px"
                    pt={{ lg: '120px' }}
                >
                    Daksanaya itu apa sih ?
                </SubTitle>
                <WhiteCard
                    w="80vw"
                    h={{ lg: "60vh", base: '40vh' }}
                    display='flex'
                    flexDir='column'
                    justifyContent="center"
                    alignItems="center"

                >
                    <Text
                        maxW="70vw"
                        align="left"
                        color="primary.darkBlueDaksa"
                        fontSize={{ lg: "1.75rem", base: '1rem' }}
                    >
                        Daksanaya Manajemen merupakan sebuah wadah pengembangan pengetahuan dan advisory dibidang keuangan dan investasi. Kami memberikan pengembangan pengetahuan melalui Training dan Workshop untuk public maupun privat (korporat) di bidang Financial Planning, Corporate Finance, Investment Portfolio and Management, Financial Statement Analysis, Stock Valuation, dan informasi pengetahuan tambahan melalui Book and Research. Kami juga memberikan jasa konsultasi keuangan oleh profesional melalui Financial Advisory.
                    </Text>
                </WhiteCard>

                <SubTitle
                    color="primary.darkBlueDaksa"
                    mt={{ lg: "100px", base: '50px' }}
                    mb={{ lg: "50px", base: '30px' }}
                >
                    Dua Misi Daksa untuk Indonesia

                </SubTitle>
                <Flex
                    flexDir={{ lg: 'row', base: 'column' }}
                    justifyContent='center'
                    alignItems='center'
                    flexWrap="wrap"
                    zIndex="9"
                >
                    <Misi
                        title="Edukasi"
                        desc="Berbagi ilmu dan informasi untuk Indonesia melek investasi"
                    />
                    <Misi
                        title="Konsultasi"
                        desc="Berbagi ilmu dan informasi untuk Indonesia melek investasi"
                    />
                </Flex>
            </Box>
            <Box
                w="100vw"
                h={{ lg: "900px", base: '700px' }}
                borderRadius={{ lg: "150px 150px 0px 0px", base: '50px 50px 0px 0px' }}
                bgColor='white'
                mt={{ lg: '-200px', base: '-200px' }}
            >
                <SubTitle color="primary.darkBlueDaksa" pt={{ lg: "230px", base: '175px' }} mb={{ lg: "100px", base: '50px' }} maxW={{ base: '70vw' }} mx="auto"> Dokumentasi Kegiatan Kami</SubTitle>
                <ElasticCarousel />
            </Box>



        </>
    )
}

export default Profil;