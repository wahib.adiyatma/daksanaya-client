import Carousel from "react-multi-carousel";
import { Image } from "@chakra-ui/image"
import Content from "./Content";
import { Box, HStack, Center } from "@chakra-ui/layout";

const responsive = {
    desktop: {
        breakpoint: { max: 4000, min: 1200 },
        items: 2,
        paritialVisibilityGutter: 100
    }
};
const images = [
    "https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1549396535-c11d5c55b9df?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",

];

// Because this is an inframe, so the SSR mode doesn't not do well here.
// It will work on real devices.
const MultiCarousel = ({ ...props }) => {
    return (
        <Box
            {...props}
        >
            <Carousel
                ssr={true}
                infinite={true}
                partialVisbile={true}
                responsive={responsive}
                w="100vw"
            >
                <Content />
                <Content />
                <Content />
                <Content />
            </Carousel >
        </Box>

    );
};

export default MultiCarousel;

