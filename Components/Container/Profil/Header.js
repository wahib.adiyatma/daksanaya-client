import { Box } from "@chakra-ui/layout";

const Header = ({ children, ...props }) => {
    return (
        <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            bgImage="/images/profile/header.png"
            bgSize="cover"
            bgRepeat="norepeat"
            minW="100vw"
            minH={{ lg: '765px', base: '350px' }}
            {...props}
        >
            {children}
        </Box >
    );
}

export default Header;