import { ErrorToast } from '../Components/Toast';

// pass errors from react-hook-form
export const toastErrorMessage = ({ errors, title }) => {
    let foundNotif = false;
    for (const errParent of Object.values(errors)) {
        for (const key in errParent) {
            if (key !== 'message') continue;
            ErrorToast(title, errParent[key]);
            foundNotif = true;
            break;
        }

        if (foundNotif) break;
    }
};
