import axios from 'axios';
import {
    allEvent,
    allAcademyForm,
    allConsultation,
    allContact,
    detailEvents,
    allTestimony,
    allGalleries
} from './endpoints';

export const eventApi = {
    getAllEvents: async () => {
        try {
            const res = await axios.get(allEvent);
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
    getDetailEvents: async () => {
        try {
            const res = await axios.get(detailEvents());
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
};

export const contactApi = {
    addContact: async (dataPost) => {
        try {
            const res = await axios.post(allContact, dataPost);
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
};

export const consultationApi = {
    addConsultation: async (dataPost) => {
        try {
            const res = await axios.post(allConsultation, dataPost);
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
};

export const academyFormApi = {
    addAcademyForm: async (dataPost) => {
        try {
            const res = await axios.post(allAcademyForm, dataPost);
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
};

export const testimonyApi = {

    getAllTestimonies: async () => {
        try {
            const res = await axios.get(allTestimony);
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
}

export const galleriesApi = {

    getAllGalleries: async () => {
        try {
            const res = await axios.get(allGalleries);
            const data = await res.data;
            return data;
        } catch (error) {
            throw new Error(error);
        }
    },
}