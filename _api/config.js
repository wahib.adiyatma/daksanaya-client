const config = {
    production: {
        API_BASE_URL: 'https://daksanaya.herokuapp.com/api',
        STORAGE_BASE_URL: 'https://daksanaya.herokuapp.com',
        BASE_URL: '/',

    },
    development: {
        API_BASE_URL: 'http://localhost:1337/api',
        STORAGE_BASE_URL: 'http://localhost:1337',
        BASE_URL: '/',
    },
};
export default {
    ...config[process.env.NODE_ENV || development],
};
