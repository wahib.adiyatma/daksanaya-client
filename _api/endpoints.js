import axios from 'axios';
import config from './config';

axios.defaults.baseURL = config.API_BASE_URL;
//Events
export const allEvent = '/events';
export const detailEvents = (eventId) => `/events/${eventId}`;

//Consultation
export const allConsultation = '/consultations';

//Contact
export const allContact = '/contacts';

//academy-form
export const allAcademyForm = '/academy-forms';

//testimony
export const allTestimony = '/testimonies'

//gallery
export const allGalleries = '/galleries'