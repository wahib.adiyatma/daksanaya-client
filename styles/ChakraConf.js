import { extendTheme } from '@chakra-ui/react';
import { createBreakpoints } from '@chakra-ui/theme-tools';

const styles = {
    global: {
        // styles for the `body`
        body: {
            fontFamily: 'Barlow',
        },
        '*': {
            margin: 0,
            padding: 0,
        },
    },
};

const breakpoints = createBreakpoints({
    sm: '376px',
    semiMd: '480px',
    md: '754px',
    lg: '944px',
    xl: '1024px',
});

// 2. Extend the theme to include custom colors, fonts, etc
export const colors = {
    primary: {
        blueDaksa: '#0B62FF',
        darkBlueDaksa: '#0E3D67',
        blueDaksaBg: '#508BFF',
        grayDaksa: '#626669',
        blackDaksa: '#0E1318',
        blackDaksa2: '#141B24',
        whiteDaksa: '#F8F8F8'
    },
};

const theme = extendTheme({
    breakpoints,
    styles,
    colors,
});

export default theme;
